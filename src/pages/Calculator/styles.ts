import styled from "styled-components/native";

export const Container = styled.View`
  flex: 1;
  background-color: black;
`;

export const ContainerResult = styled.View`
  flex: 3;
  align-items: flex-end;
  justify-content: flex-end;
  padding: 20px;
`;

export const Result = styled.Text`
  font-size: 26px;
  color: white;
  font-weight: bold;
`;

export const ContainerKeyboard = styled.View`
  flex: 7;
  flex-direction: row;
  flex-wrap: wrap;
  justify-content: flex-end;
`;
