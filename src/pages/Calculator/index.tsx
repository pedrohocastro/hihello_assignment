import React, { useState } from "react";

import * as S from "./styles";
import { CalculatorButton } from "../../components/Buttons/CalculatorButton";
import { Alert } from "react-native";

export function Calculator() {
  const [firstNumber, setFirstNumber] = useState("");
  const [secondNumber, setSecondNumber] = useState("");
  const [operator, setOperator] = useState("");
  const [result, setResult] = useState("");
  const buttons = [
    "C",
    "±",
    "%",
    "/",
    "7",
    "8",
    "9",
    "x",
    "4",
    "5",
    "6",
    "-",
    "1",
    "2",
    "3",
    "+",
    "0",
    ".",
    "=",
  ];

  function handlePress(symbol: string) {
    if (result) {
      setResult("");
    }
    if (/^\d+$/.test(symbol)) {
      handleNumberPress(symbol);
      return;
    }
    if (symbol === "+" || symbol === "-" || symbol === "/" || symbol === "x") {
      handleOperator(symbol);
    }

    if (symbol === "C") {
      clearCalculator();
    }
    if (symbol === "=") {
      calculateResult();
    }
  }
  function handleNumberPress(value: string) {
    if (!operator) {
      setFirstNumber((prev) => prev + value);
    } else {
      setSecondNumber((prev) => prev + value);
    }
  }

  function handleOperator(value: string) {
    if (firstNumber) {
      setOperator(value);
    }
  }

  function calculateResult() {
    if (firstNumber && secondNumber && operator) {
      const num1 = parseFloat(firstNumber);
      const num2 = parseFloat(secondNumber);

      switch (operator) {
        case "+":
          setResult((num1 + num2).toString());
          break;
        case "-":
          setResult((num1 - num2).toString());
          break;
        case "x":
          setResult((num1 * num2).toString());
          break;
        case "/":
          setResult((num1 / num2).toString());
          break;
        default:
          setResult("");
      }
    }
  }

  function clearCalculator() {
    setFirstNumber("");
    setSecondNumber("");
    setOperator("");
    setResult("");
  }

  function getDisplayedNumber() {
    if (result) {
      return result || "0";
    }
    if (operator) {
      return secondNumber || "0";
    }
    return firstNumber || "0";
  }

  return (
    <S.Container>
      <S.ContainerResult>
        <S.Result>{getDisplayedNumber()}</S.Result>
      </S.ContainerResult>
      <S.ContainerKeyboard>
        {buttons.map((button) => (
          <CalculatorButton
            title={button}
            key={button}
            style={{ width: button == "0" ? "50%" : "25%" }}
            onPress={() => handlePress(button)}
          />
        ))}
      </S.ContainerKeyboard>
    </S.Container>
  );
}
