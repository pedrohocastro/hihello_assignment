import styled from "styled-components/native";

export const Container = styled.TouchableOpacity`
  min-height: 60px;
  max-height: 60px;
  width: 25%;
  align-items: center;
  justify-content: center;
  background-color: gray;
  border-width: 1px;
`;

export const Title = styled.Text`
  font-size: 20px;
  font-weight: bold;
  color: white;
`;
