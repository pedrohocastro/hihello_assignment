# React Native Calculator

This repository contains a React Native project for a simple calculator, developed as part of an assignment sent by HiHello, a company that I greatly admire and would be thrilled to work with.

## Objective

The objective of this project was to create a functional calculator within a short time frame. I remained faithful to the proposal of spending only 2 hours on development, so not all features and the design are fully completed.

## Implemented Features

- Basic operations of addition, subtraction, multiplication, and division.
- Numeric buttons for entering values.
- Equal button to perform calculations.
- Clear the calculator with the C button.

## Pending Features

There are two functionalities that have not been implemented yet:

1. Switch button to toggle a number between positive and negative.
2. Implement a decimal point "." button for decimal numbers.
3. Better UI

---

Please feel free to reach out if you have any questions or need further information.

I'm here to help!

Best regards
